/**
 * Controle do menu mobile
 */

const nav = {
    menu: _('#mainNav'),
    toggler: _('#navToggle'),
};

function navHidden(evt) {
    nav.menu.classList.remove('visible');
    nav.toggler.classList.remove('visible');
    setTimeout(() => {
        _evt(nav.menu, 'click', navHidden, !0);
        _evt(window, 'resize orientationchange', navHidden, !0);
    }, 100);
}

_evt(nav.toggler, 'click', () => {
    nav.menu.classList.add('visible');
    nav.toggler.classList.add('visible');
    _evt(nav.menu, 'click', navHidden);
    _evt(window, 'resize orientationchange', navHidden);
});



/**
 * WhatsApp API
 */
function whatsapp(e) {
    _isMobile() || (e.removeAttribute("onmouseover"), e.href = e.href.replace("api", "web"))
}


/**
 * Image Lazy
 */
 _lazyView('*.img-lazy', (e) => {
    e.src = e.dataset.src;
    e.classList.remove('img-blur');
    setTimeout(function () {
        e.classList.remove('img-lazy');
    }, 1000);
}, 2)