"use strict";

/**
 * @param {string} e 
 * @param {string|Object} p 
 * @returns 
 */
function _(e, p) {
    let g;
    p = typeof p === 'string' ? _(p) : !p ? document : p;
    switch (e.substring(0, 1)) {
        case '#':
            g = 'getElementById';
            break;
        case '.':
            g = 'getElementsByClassName';
            break;
        case '$':
            g = 'querySelector';
            break;
        case '*':
            g = 'querySelectorAll';
            break;
        default:
            g = 'getElementsByTagName';
    }
    return p[g](e.substr(0, 1).match(/^[a-zA-Z-0-9]+$/) ? e : e.substring(1));
}

/**
 * Adiciona ouvintes com addEventListener
 * @param {object} e Elemento alvo
 * @param {string} t Tipos de eventos separados por espaço
 * @param {function} f Método ouvinte
 * @param {boolean} r Se verdadeiro remove o evento. Use !0;
 * @param {*} c Captura do evento
 */
function _evt(e, t, f, r, c) {
    const s = t.split(" "),
        l = s.length;
    for (let i = 0; i < l; i++) {
        e[r ? "removeEventListener" : "addEventListener"](s[i], f, c)
    }
}


/**
 * @param {Object} params 
 */
function _ajax(params) {
    const isFormData = String(params.data).indexOf("FormData") !== -1,
        header = isFormData ? "X-Requested-With" : "Content-type",
        contentType = isFormData ? "XMLHttpRequest" : "application/json; charset=utf-8",
        xhr = new(window.XMLHttpRequest || ActiveXObject("MSXML2.XMLHTTP.3.0"))();

    _evt(xhr.upload, "abort error load loadend loadstart progress", params.upload);
    _evt(xhr, "readystatechange", function () {
        this.readyState === 4 &&
            (this.status === 200 ? params.success && params.success(this) : params.error && params
                .error(this));
    });

    xhr.open(params.method || "GET", params.url || window.location.href, !0, params.user, params.password);
    xhr.setRequestHeader(header, params.contentType || contentType);
    xhr.send(typeof params.data != "object" || isFormData ? params.data : JSON.stringify(params.data));
    params.loading && params.loading();
}

/**
 * @param {Object} params 
 */
function _submit(params) {

    _evt(params.form, "submit", function (e) {
        e.preventDefault();
        _ajax({
            url: e.currentTarget.action,
            method: params.method || e.currentTarget.method,
            loading: params.loading,
            success: function (r) {
                params.success && data.success(r.responseText)
            },
            upload: function (r) {
                params.upload && data.upload(r)
            },
            params: new FormData(e.currentTarget)
        })

        params.reset && params.form.reset()
    })
}


function _isMobile() {
    return (/iphone|ipod|android|ie|blackberry|fennec/).test(navigator.userAgent.toLowerCase())
}


/**
 * Atrasa o carregamento de elementos no DOM
 * @param {*} elements 
 * @param {*} callback 
 * @param {*} point 
 */
 function _lazyView(elements, callback, point) {

    'use strict';

    function lazyLoadElement(e) {
        function checkView() {
            if (pointView(e)) {
                callback(e);
                _evt(document, 'scroll', checkView, !0);
                _evt(window, 'resize orientationchange', checkView, !0);
            }
        }
        _evt(document, 'DOMContentLoaded scroll', checkView);
        _evt(window, 'resize orientationchange', checkView);
        checkView();
    }

    // Verifica se imagem está no ponto de visualização
    function pointView(e) {
        return (e.getBoundingClientRect().top + (e.getBoundingClientRect().height / (point || 10))) <= (window.innerHeight || document.documentElement.clientHeight) && e.getBoundingClientRect().bottom >= 0;
    }

    function loadElements(e) {
        if ('*' != e.substring(0, 1)) return new lazyLoadElement(_(e)), !1;
        let listElements = _(e),
            totalElements = listElements.length;
        for (let i = 0; i < totalElements; i++) {
            new lazyLoadElement(listElements[i]);
        }
    }
    loadElements(elements);
}
